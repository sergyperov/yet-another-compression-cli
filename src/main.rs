use clap::Clap;
use clap::AppSettings::ArgRequiredElseHelp;
use std::io::{Read, Write};
use std::fs;
use algorithms::{rle_compress, rle_decompress};

enum CompressModes {
    A,
    B
}

#[derive(Clap)]
#[clap(version = "1.0", author = "Sergy P. <sergyperov@gmail.com>", global_setting = ArgRequiredElseHelp)]
struct Opts {
    input: String,
    output: String,
    #[clap(short, takes_value=false)]
    decompress: bool,
}

fn main() {
    let opts: Opts = Opts::parse();
    let input_file_wrapped = fs::File::open(opts.input.clone());

    match input_file_wrapped {
        Ok(mut input_file) => {
            if opts.decompress {
                decompress(&mut input_file, opts.input.clone(), opts.output.clone());
            } else {
                compress(&mut input_file, opts.input.clone(), opts.output.clone());
            }
        },
        Err(_) => {
            println!("Error reading file \"{}\", exiting...", opts.input.clone());
        }
    }
}

fn decompress(input_file: &mut fs::File, input_fname: String, output_fname: String) {
    println!("Decompressing file \"{}\" using RLE", input_fname.clone());
    let mut buffer: Vec<u8> = Vec::new();
    input_file.read_to_end(&mut buffer);
    let compressed = rle_decompress(buffer.as_slice());
    let result = fs::File::create(output_fname.clone());
    result.unwrap().write_all(compressed.as_slice());
    println!("Decompressed file is saved as \"{}\"", output_fname.clone());
}

fn compress(input_file: &mut fs::File, input_fname: String, output_fname: String) {
    println!("Compressing file \"{}\" using RLE", input_fname.clone());
    let mut buffer: Vec<u8> = Vec::new();
    input_file.read_to_end(&mut buffer);
    let compressed = rle_compress(buffer.as_slice());
    let result = fs::File::create(output_fname.clone());
    result.unwrap().write_all(compressed.as_slice());
    println!("Compressed file is saved as \"{}\"", output_fname.clone());
    println!("Original size: {}B, compressed size: {}B", buffer.len(), compressed.len());
}