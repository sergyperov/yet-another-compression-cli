use std::ops::Deref;
use std::borrow::Borrow;

const MAX_COUNTER: usize = 127;

/// [Алгоритм RLE - RunLengthEncoding, компрессия]
pub fn rle_compress(input: &[u8]) -> Vec<u8> {
    // итоговый результат, в котором будет сжатая строка
    let mut result: Vec<u8> = Vec::new();
    // временное хранение серий разных байт
    let mut cache: Vec<u8> = Vec::new();

    // сразу записываем текущий элемент как "прошлый", ставим счётчик на 1 и добавляем его в кэш
    let mut prev: u8 = input[0];
    let mut counter: i8 = 1;    // счётчик идущ подряд одинаковых байт
    cache.push(prev);

    for i in 1..input.len() {
        let current = input[i];
        // два основных ветвления - текущий байт равен прошлому или нет
        if current == prev {
            if cache.len() >= 2 {
                let unique_len = cache.len() - 1;
                push_bytes_from_cache(&mut result, &mut cache, unique_len);
            }

            if (counter as usize) < MAX_COUNTER {
                // если одинаковых символов меньше чем максиум, продолжаем инкремент
                counter += 1;
            } else {
                // иначе записываем длину и содержимое серии
                result.push(counter as u8);
                result.push(prev);
                counter = 1;
            }
        } else {
            // аналогичная логика, только вставляем наоброт серию разных байт
            if counter > 1 {
                result.push(counter as u8);
                result.push(prev);
                cache.clear();
            } else if cache.len() == MAX_COUNTER {
                let unique_len = cache.len();
                push_bytes_from_cache(&mut result, &mut cache, unique_len);
            }

            cache.push(current);
            counter = 1;
        }

        prev = current;
    }

    // не забываем про последний байт (или последнию серию байтов)
    if cache.len() >= 2 {
        let unique_len = cache.len();
        push_bytes_from_cache(&mut result, &mut cache, unique_len);
    } else {
        result.push(counter as u8);
        result.push(prev);
    }

    result
}

/// добавляем первые `n` элементов `cache` в `result`, очищая `cache`
fn push_bytes_from_cache(result: &mut Vec<u8>, cache: &mut Vec<u8>, n: usize) {
    result.push(((n as i8) * (-1)) as u8);
    for i in 0..n {
        result.push(cache[i]);
    }
    cache.clear();
}

/// [Алгоритм RLE - RunLengthEncoding, декомпрессия]
pub fn rle_decompress(input: &[u8]) -> Vec<u8> {
    let mut result: Vec<u8> = Vec::new();
    let mut flag: i8 = 0;

    for &b in input {
        if flag == 0 {
            flag = (b as i8);
            continue;
        }

        if flag > 0 {
            while flag > 0 {
                result.push(b);
                flag -= 1;
            }
        }

        if flag < 0 {
            result.push(b);
            flag += 1;
        }
    }

    result
}