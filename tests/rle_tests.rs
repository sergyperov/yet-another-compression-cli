#[cfg(test)]
mod tests {
    use std::str;
    use algorithms::{rle_compress, rle_decompress};

    #[test]
    fn sequence_of_different_symbols() {
        let compressed = rle_compress(b"abcde");
        assert_eq!(compressed, [
            -5i8 as u8, 97, 98, 99, 100, 101
        ]);
    }

    #[test]
    fn sequence_of_same_symbols() {
        let compressed = rle_compress(b"aaa");
        assert_eq!(compressed, [
            3, 97
        ]);
    }

    #[test]
    fn two_sequences_of_same_symbols() {
        let compressed = rle_compress(b"aaabbb");
        assert_eq!(compressed, [
            3, 97,
            3, 98
        ]);
    }

    #[test]
    fn same_symbols_and_single() {
        let compressed = rle_compress(b"aaab");
        assert_eq!(compressed, [
            3, 97,
            1, 98
        ]);
    }

    #[test]
    fn same_symbols_and_different_ones() {
        let compressed = rle_compress(b"aaabc");
        assert_eq!(compressed, [
            3, 97,
            -2i8 as u8, 98, 99
        ]);
    }

    #[test]
    fn single_symbol_and_sequence() {
        let compressed = rle_compress(b"abbb");
        assert_eq!(compressed, [
            -1i8 as u8, 97,
            3, 98
        ]);
    }

    #[test]
    fn different_symbols_and_sequence() {
        let compressed = rle_compress(b"abcdddd");
        assert_eq!(compressed, [
            -3i8 as u8, 97, 98, 99,
            4, 100
        ]);
    }

    #[test]
    fn complex_case() {
        let compressed = rle_compress(b"aabbbcabcabceeefff");
        assert_eq!(compressed, [
            2, 97,
            3, 98,
            -7i8 as u8, 99, 97, 98, 99, 97, 98, 99,
            3, 101,
            3, 102
        ]);
    }

    #[test]
    fn buffer_max_size_same_symbols() {
        let compressed = rle_compress("a".repeat(127).as_bytes());
        assert_eq!(compressed, [
            127, 97
        ]);
    }

    #[test]
    fn buffer_overflow_same_symbols() {
        let compressed = rle_compress("a".repeat(128).as_bytes());
        assert_eq!(compressed, [
            127, 97,
            1, 97
        ]);
    }

    #[test]
    fn several_chinks_same_symbols() {
        let compressed = rle_compress("a".repeat(300).as_bytes());
        assert_eq!(compressed, [
            127, 97,
            127, 97,
            46, 97
        ]);
    }

    #[test]
    fn buffer_max_size_different_symbols() {
        let str = format!("{}a", "abc".repeat(42));     // len = 127
        let compressed = rle_compress(str.as_bytes());

        let mut correct = [97, 98, 99].repeat(42);
        correct.push(97);
        // [-127, 97, 98, 99, ..., 99, 97]
        correct.insert(0, -127i8 as u8);
        assert_eq!(compressed, correct);
    }

    #[test]
    fn buffer_overflow_different_symbols() {
        let str = format!("{}ab", "abc".repeat(42));     // len = 128
        let compressed = rle_compress(str.as_bytes());

        let mut correct = [97, 98, 99].repeat(42);
        correct.push(97);
        correct.push(1 as u8);
        correct.push(98);
        // [-127, 97, 98, 99, ..., 99, 97, 1, 98]
        correct.insert(0, -127i8 as u8);
        assert_eq!(compressed, correct);
    }

    #[test]
    fn full_cycle_different_symbols() {
        let str = "abcde";
        assert_eq!(str, compress_and_decompress(str));
    }

    #[test]
    fn full_cycle_same_symbols() {
        let str = "aaaaa";
        assert_eq!(str, compress_and_decompress(str));
    }

    #[test]
    fn full_cycle_different_symbols_long() {
        let str = "abababab";
        assert_eq!(str, compress_and_decompress(str));
    }

    #[test]
    fn full_cycle_mashed() {
        let str = "aaaabbbbqwertyiii";
        assert_eq!(str, compress_and_decompress(str));
    }

    #[test]
    fn full_cycle_long() {
        let str = "abccccc".repeat(100);
        assert_eq!(str, compress_and_decompress(str.as_ref()));
    }

    fn compress_and_decompress(str: &str) -> String {
        let compressed = rle_compress(str.as_bytes());
        let decompressed = rle_decompress(compressed.as_slice());

        String::from(str::from_utf8(decompressed.as_slice()).unwrap())
    }
}